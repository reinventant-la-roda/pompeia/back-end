rootProject.name = "pompeia-bookstore"

include(":app")
include(":domain")

include(":repository-domain")
project(":repository-domain").projectDir = file("repository/domain")

include(":repository-application")
project(":repository-application").projectDir = file("repository/application")

include(":repository-infrastructure")
project(":repository-infrastructure").projectDir = file("repository/infrastructure")

include(":repository-restApi-domain")
project(":repository-restApi-domain").projectDir = file("repository/restApi/domain")

include(":repository-restApi-infrastructure")
project(":repository-restApi-infrastructure").projectDir = file("repository/restApi/infrastructure")
