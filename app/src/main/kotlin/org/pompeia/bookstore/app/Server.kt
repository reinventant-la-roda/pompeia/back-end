package org.pompeia.bookstore.app

import org.pompeia.bookstore.domain.Book
import org.pompeia.bookstore.repository.application.Create
import org.pompeia.bookstore.repository.application.Delete
import org.pompeia.bookstore.repository.application.Read
import org.pompeia.bookstore.repository.application.Update
import org.pompeia.bookstore.repository.domain.Repository
import org.pompeia.bookstore.repository.infrastructure.MemoryRepository
import org.pompeia.bookstore.repository.restApi.infrastructure.KtorServer

fun main() {
    val repository: Repository = MemoryRepository()
    repository.add(Book(isbn = "054792822X", title = "The Hobbit", authors = listOf("J. R. R. Tolkien")))

    KtorServer(
        create = Create(repository),
        read = Read(repository),
        update = Update(repository),
        delete = Delete(repository)
    ).run(9090)
}
