plugins {
    kotlin("jvm")
    application
}

dependencies {
    api(project(":repository-restApi-infrastructure"))
    implementation(project(":repository-infrastructure"))
    implementation(project(":repository-application"))
}

application {
    mainClass.set("org.pompeia.bookstore.app.ServerKt")
}
