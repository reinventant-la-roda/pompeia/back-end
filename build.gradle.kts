plugins {
    kotlin("jvm") version "1.6.21" apply false
    kotlin("plugin.serialization") version "1.6.21" apply false
    id("io.kotest") version "0.3.8" apply false
}

subprojects {
    group = "org.pompeia.bookstore"

    repositories {
        mavenCentral()
    }

    tasks {
        withType<Jar> {
            archiveBaseName.set("${rootProject.name}-${project.name}")
        }
    }
}
