#!/bin/bash -xe

curl -v \
    --request PUT \
    http://localhost:9090/api/book \
    --header 'Content-Type: application/json' \
    -d '{
      "isbn": "4567890123",
      "title": "curios",
      "authors": [
        "Francesc", "Reinoso"
      ]
    }'
