#!/bin/bash -xe

curl -v \
    --request GET \
    http://localhost:9090/api/book/${1:-054792822X} \
    | jq
