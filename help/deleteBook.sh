#!/bin/bash -xe

curl -v \
    --request DELETE \
    http://localhost:9090/api/book/${1:-4567890123}
