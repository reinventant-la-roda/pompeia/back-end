package org.pompeia.bookstore.domain

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.string.shouldEndWith

class BookTest : FreeSpec({
    "creating" - {
        "happy case" {
            Book(
                isbn = "054792822X",
                title = "The Hobbit",
                authors = listOf("J. R. R. Tolkien")
            )
        }

        "without authors" {
            Book(
                isbn = "054792822X",
                title = "The Hobbit",
                authors = listOf()
            )
        }

        "directly with objects" {
            val isbn = BookId.of("054792822X")
            val title = BookTitle("The Hobbit")
            val author = Author("J. R. R. Tolkien")

            Book(isbn, title, listOf(author))
        }

        "empty value" - {
            "isbn" {
                val exception = shouldThrow<EmptyValue> {
                    Book(
                        isbn = "",
                        title = "The Hobbit",
                        authors = listOf("J. R. R. Tolkien")
                    )
                }

                exception.message shouldEndWith "ISBN"
            }
            "title" {
                val exception = shouldThrow<EmptyValue> {
                    Book(
                        isbn = "054792822X",
                        title = "",
                        authors = listOf("J. R. R. Tolkien")
                    )
                }

                exception.message shouldEndWith "book title"
            }
            "author" {
                val exception = shouldThrow<EmptyValue> {
                    Book(
                        isbn = "054792822X",
                        title = "The Hobbit",
                        authors = listOf("")
                    )
                }

                exception.message shouldEndWith "author name"
            }
        }
    }
})
