package org.pompeia.bookstore.domain

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.FreeSpec
import io.kotest.datatest.withData
import io.kotest.matchers.string.shouldEndWith

class BookIdTest : FreeSpec({
    "creating" - {
        "happy case" - {
            withData(
                "054792822X",
                "978-0547928227",
                "9780547928227"
            ) {
                BookId.of(it)
            }
        }

        "empty value" {
            val exception = shouldThrow<EmptyValue> {
                BookId.of("")
            }

            exception.message shouldEndWith "ISBN"
        }

        "unexpected length of ean" {
            val notEan = "Is not a valid ean"
            val exception = shouldThrow<UnexpectedLength> {
                BookId.of(notEan)
            }

            exception.message shouldEndWith notEan.length.toString()
        }
    }
})
