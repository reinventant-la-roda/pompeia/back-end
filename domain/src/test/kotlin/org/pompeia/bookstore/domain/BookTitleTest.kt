package org.pompeia.bookstore.domain

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.string.shouldEndWith

class BookTitleTest : FreeSpec({
    "creating" - {
        "happy case" {
            BookTitle("The Hobbit")
        }

        "empty value" {
            val exception = shouldThrow<EmptyValue> {
                BookTitle("")
            }

            exception.message shouldEndWith "book title"
        }
    }
})
