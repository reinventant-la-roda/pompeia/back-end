package org.pompeia.bookstore.domain

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.string.shouldEndWith

class AuthorTest : FreeSpec({
    "creating" - {
        "happy case" {
            Author("J. R. R. Tolkien")
        }

        "empty value" {
            val exception = shouldThrow<EmptyValue> {
                Author("")
            }

            exception.message shouldEndWith "author name"
        }
    }
})
