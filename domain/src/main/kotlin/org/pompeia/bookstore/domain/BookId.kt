package org.pompeia.bookstore.domain

data class BookId private constructor(val isbn: String) {
    init {
        if (isbn.isEmpty()) throw EmptyValue("ISBN")
        if (isbn.length !in listOf(10, 13)) throw UnexpectedLength(isbn)
    }

    companion object {
        fun of(isbn: String): BookId = isbn.filter { it != '-' }.let(::BookId)
    }

    override fun toString(): String = "ISBN: '$isbn'"
}
