package org.pompeia.bookstore.domain

class UnexpectedLength(isbn: String) : Exception("Unexpected length for ISBN. Expected 10 or 13 but this value '$isbn' are ${isbn.length}")
