package org.pompeia.bookstore.domain

data class BookTitle(val title: String) {
    init {
        if (title.isEmpty()) throw EmptyValue("book title")
    }
}
