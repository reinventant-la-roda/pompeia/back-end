package org.pompeia.bookstore.domain

data class Author(val name: String) {
    init {
        if (name.isEmpty()) throw EmptyValue("author name")
    }
}
