package org.pompeia.bookstore.domain

class EmptyValue(key: String) : Exception("Has a empty value for $key")
