package org.pompeia.bookstore.domain

data class Book(val isbn: BookId, val title: BookTitle, val authors: List<Author>) {
    constructor(isbn: String, title: String, authors: List<String>) : this(isbn = BookId.of(isbn), title = BookTitle(title), authors = authors.map(::Author))
}
