val kotest = "5.3.0"

plugins {
    kotlin("jvm")
    id("io.kotest")
}

dependencies {
    testImplementation(libs.bundles.kotest)
    testImplementation(libs.kotest.framework.data)
}
