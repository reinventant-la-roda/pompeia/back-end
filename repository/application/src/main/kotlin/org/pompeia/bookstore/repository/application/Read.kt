package org.pompeia.bookstore.repository.application

import org.pompeia.bookstore.domain.BookId
import org.pompeia.bookstore.repository.domain.Repository
import org.pompeia.bookstore.restApi.domain.BookSerializable

class Read(private val repository: Repository) {
    fun execute(isbn: String): BookSerializable =
        isbn
            .let(BookId::of)
            .let(repository::get)
            .let(::BookSerializable)
}
