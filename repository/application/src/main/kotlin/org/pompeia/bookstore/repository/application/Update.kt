package org.pompeia.bookstore.repository.application

import org.pompeia.bookstore.repository.domain.Repository
import org.pompeia.bookstore.restApi.domain.BookSerializable

class Update(private val repository: Repository) {
    fun execute(book: BookSerializable) {
        book
            .toBook()
            .let(repository::update)
    }
}
