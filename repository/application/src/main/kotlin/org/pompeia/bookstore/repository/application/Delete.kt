package org.pompeia.bookstore.repository.application

import org.pompeia.bookstore.domain.BookId
import org.pompeia.bookstore.repository.domain.Repository

class Delete(private val repository: Repository) {
    fun execute(isbn: String) {
        isbn
            .let(BookId::of)
            .let(repository::delete)
    }
}
