package org.pompeia.bookstore.repository.application

import org.pompeia.bookstore.restApi.domain.BookSerializable
import org.pompeia.bookstore.repository.domain.Repository

class Create(private val repository: Repository) {
    fun execute(book: BookSerializable) {
        book
            .toBook()
            .let(repository::add)
    }
}
