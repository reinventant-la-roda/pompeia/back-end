plugins {
    kotlin("jvm")
}

dependencies {
    api(project(":repository-restApi-domain"))
    implementation(project(":repository-domain"))
}
