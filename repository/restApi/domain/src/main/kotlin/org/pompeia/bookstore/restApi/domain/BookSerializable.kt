package org.pompeia.bookstore.restApi.domain

import kotlinx.serialization.Serializable
import org.pompeia.bookstore.domain.Book

@Serializable
data class BookSerializable(val isbn: String, val title: String, val authors: List<String>) {
    constructor(book: Book) : this(
        isbn = book.isbn.isbn,
        title = book.title.title,
        authors = book.authors.map { it.name }
    )

    fun toBook() = Book(isbn = isbn, title = title, authors = authors)

    companion object {
        const val path = "/book"
    }
}
