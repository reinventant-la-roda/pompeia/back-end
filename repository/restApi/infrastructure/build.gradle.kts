val ktorVersion = "2.0.1"
val logbackVersion = "1.2.10"

plugins {
    kotlin("jvm")
}

dependencies {
    api(project(":repository-restApi-domain"))
    implementation(project(":repository-application"))
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")
    implementation("io.ktor:ktor-server-compression:$ktorVersion")
    implementation("io.ktor:ktor-server-content-negotiation:$ktorVersion")
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
}
