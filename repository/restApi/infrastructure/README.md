- Example from: https://play.kotlinlang.org/hands-on/Full%20Stack%20Web%20App%20with%20Kotlin%20Multiplatform/03_A_Simple_API_Server
- CRUD: https://codebots.com/crud/how-does-crud-relate-to-a-rest-api

| CRUD    | HTTP    | REST            |
|---------|---------|-----------------|
| Create  | POST    | /api/book       |
| Read    | GET     | /api/book/{id}  |
| Update  | PUT     | /api/book       |
| Delete  | DELETE  | /api/book/{id}  |
