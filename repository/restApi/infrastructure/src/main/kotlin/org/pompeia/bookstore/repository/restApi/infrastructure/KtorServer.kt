package org.pompeia.bookstore.repository.restApi.infrastructure

import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.compression.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.pompeia.bookstore.repository.application.Create
import org.pompeia.bookstore.repository.application.Delete
import org.pompeia.bookstore.repository.application.Read
import org.pompeia.bookstore.repository.application.Update
import org.pompeia.bookstore.restApi.domain.BookSerializable

class KtorServer(
    private val create: Create,
    private val read: Read,
    private val update: Update,
    private val delete: Delete
) {
    fun run(port: Int = 9090) {
        embeddedServer(Netty, port) {
            install(ContentNegotiation) {
                json()
            }
            install(Compression) {
                gzip()
            }
            routing {
                route("/api${BookSerializable.path}") {
                    post {
                        call.receive<BookSerializable>()
                            .let(create::execute)
                        call.respond(HttpStatusCode.OK)
                    }
                    get("/{id}") {
                        (call.parameters["id"] ?: TODO("Exception Not Implemented yet"))
                            .let(read::execute)
                            .let { call.respond(it) }
                    }
                    put {
                        call.receive<BookSerializable>()
                            .let(update::execute)
                        call.respond(HttpStatusCode.OK)
                    }
                    delete("/{id}") {
                        (call.parameters["id"] ?: TODO("Exception Not Implemented yet"))
                            .let(delete::execute)
                        call.respond(HttpStatusCode.OK)
                    }
                }
            }
        }.start(wait = true)
    }
}
