package org.pompeia.bookstore.repository.domain

import org.pompeia.bookstore.domain.BookId

class NotExists(isbn: BookId) : Exception("Can't get, delete or update the book with ISBN: $isbn")
