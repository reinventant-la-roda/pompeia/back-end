package org.pompeia.bookstore.repository.domain

import org.pompeia.bookstore.domain.BookId

class AlreadyExists(isbn: BookId) : Exception("Already exists the book with $isbn")
