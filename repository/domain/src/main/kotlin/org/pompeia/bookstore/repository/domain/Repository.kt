package org.pompeia.bookstore.repository.domain

import org.pompeia.bookstore.domain.Book
import org.pompeia.bookstore.domain.BookId

interface Repository {
    fun add(book: Book)
    fun get(id: BookId): Book
    fun update(book: Book)
    fun delete(id: BookId)
}
