package org.pompeia.bookstore.repository.infrastructure

import org.pompeia.bookstore.domain.Book
import org.pompeia.bookstore.domain.BookId
import org.pompeia.bookstore.repository.domain.AlreadyExists
import org.pompeia.bookstore.repository.domain.NotExists
import org.pompeia.bookstore.repository.domain.Repository

class MemoryRepository : Repository {
    private val repository: MutableMap<BookId, Book> = mutableMapOf()

    override fun add(book: Book) {
        val key = book.isbn
        if (repository[key] != null)
            throw AlreadyExists(key)

        repository[key] = book
    }

    override fun get(id: BookId) = repository[id] ?: throw NotExists(id)

    override fun update(book: Book) {
        val key = book.isbn
        if (repository[key] == null)
            throw NotExists(key)

        repository[key] = book
    }

    override fun delete(id: BookId) {
        repository.remove(id) ?: throw NotExists(id)
    }
}
