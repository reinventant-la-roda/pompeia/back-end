package org.pompeia.bookstore.repository.infrastructure

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import org.pompeia.bookstore.domain.Author
import org.pompeia.bookstore.domain.Book
import org.pompeia.bookstore.domain.BookId
import org.pompeia.bookstore.domain.BookTitle
import org.pompeia.bookstore.repository.domain.AlreadyExists
import org.pompeia.bookstore.repository.domain.NotExists
import org.pompeia.bookstore.repository.domain.Repository


private lateinit var repository: Repository

class MemoryRepositoryTest : FreeSpec({
    val hobbitBook = Book(
        isbn = BookId.of("054792822X"),
        title = BookTitle("The Hobbit"),
        authors = listOf("J. R. R. Tolkien").map(::Author)
    )
    val isbn13 = "978-0547928227".let(BookId::of)

    beforeTest {
        repository = MemoryRepository()
        repository.add(hobbitBook)
    }

    "add" - {
        "one element" {
            repository.get(hobbitBook.isbn) shouldBe hobbitBook
        }

        "error multiple times" {
            shouldThrow<AlreadyExists> {
                repository.add(hobbitBook)
            }.message shouldContain hobbitBook.isbn.toString()
        }
    }

    "get" - {
        "happy case" {
            repository.get(hobbitBook.isbn) shouldBe hobbitBook
        }

        "not exists this ISBN" {
            shouldThrow<NotExists> {
                repository.get(isbn13)
            }.message shouldContain isbn13.toString()
        }
    }

    "update" - {
        "happy case" {
            val updated = hobbitBook.copy(authors = listOf())
            repository.update(updated)
            repository.get(updated.isbn) shouldBe updated
        }

        "not exists this ISBN" {
            val updated = hobbitBook.copy(isbn = isbn13)
            shouldThrow<NotExists> {
                repository.update(updated)
            }.message shouldContain updated.isbn.toString()
        }
    }

    "delete" - {
        "happy case" {
            repository.delete(hobbitBook.isbn)
        }

        "not exists this ISBN" {
            shouldThrow<NotExists> {
                repository.delete(isbn13)
            }.message shouldContain isbn13.toString()
        }
    }
})
