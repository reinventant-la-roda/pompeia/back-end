plugins {
    kotlin("jvm")
    id("io.kotest")
}

dependencies {
    api(project(":repository-domain"))
    testImplementation(libs.bundles.kotest)
}
